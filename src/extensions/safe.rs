use gdnative::{
    object::{AssumeSafeLifetime, LifetimeConstraint},
    prelude::*,
};

pub trait RefExtension<T: GodotObject> {
    fn safe<'a, 'r>(&'r self) -> TRef<'a, T, Shared>
    where
        AssumeSafeLifetime<'a, 'r>: LifetimeConstraint<T::RefKind>;

    fn shared(self) -> Ref<T, Shared>;

}

impl<T: GodotObject> RefExtension<T> for Ref<T> {
    fn safe<'a, 'r>(&'r self) -> TRef<'a, T, Shared>
    where
        AssumeSafeLifetime<'a, 'r>: LifetimeConstraint<T::RefKind>,
    {
        unsafe { self.assume_safe() }
    }

    fn shared(self) -> Ref<T, Shared> {
        return self;
    }
}

pub trait TRefExtension<'a, T: GodotObject> {
    fn safe(self) -> TRef<'a, T, Shared>;
    fn shared(self) -> Ref<T, Shared>;
}

impl<'a, T: GodotObject> TRefExtension<'a, T> for TRef<'a, T, Shared> {
    fn safe(self) -> TRef<'a, T, Shared> {
        self
    }

    fn shared(self) -> Ref<T, Shared> {
        unsafe { self.assume_shared() }
    }
}

pub trait TRefNodeExtension<T: GodotObject> {
    fn safe<'a>(&'a self) -> &'a T;
    fn shared(self) -> Ref<T, Shared>;
}

impl<T: GodotObject> TRefNodeExtension<T> for T {
    fn safe<'a>(&'a self) -> &'a T {
        self
    }

    fn shared(self) -> Ref<T, Shared> {
        unsafe { self.assume_shared() }
    }
}
